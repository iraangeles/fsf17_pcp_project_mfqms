(function (){
    angular
        .module("MyApp",[
            "ui.router",
            "ngFlash",
            "ngSanitize",
            "ngMessages",
            "nvd3"
            ]);
})();