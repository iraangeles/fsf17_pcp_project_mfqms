/*
-- Query: SELECT * FROM ssdb.users_profiles
LIMIT 0, 1000

-- Date: 2017-04-05 12:43
*/
INSERT INTO `users_profiles` (`id`,`first_name`,`last_name`,`email`,`password`,`status`,`approval_token`,`reset_token`,`access_control`) VALUES (1,'Isaias','Angeles','isaias@sg.ibm.com','Abcd!234','Active',NULL,NULL,'Admin');
INSERT INTO `users_profiles` (`id`,`first_name`,`last_name`,`email`,`password`,`status`,`approval_token`,`reset_token`,`access_control`) VALUES (2,'Reynaldo','Corral','reiolicor@gmail.com','Abcd!234','Active',NULL,NULL,'Reader');
INSERT INTO `users_profiles` (`id`,`first_name`,`last_name`,`email`,`password`,`status`,`approval_token`,`reset_token`,`access_control`) VALUES (3,'Paul','Zulpa','pzulpa@gmail.com','Abcd!234','Active',NULL,NULL,'Owner');
INSERT INTO `users_profiles` (`id`,`first_name`,`last_name`,`email`,`password`,`status`,`approval_token`,`reset_token`,`access_control`) VALUES (4,'Jeff','Kumatsu','jkumatsu@gmail.com','Abcd!234',NULL,NULL,NULL,NULL);
INSERT INTO `users_profiles` (`id`,`first_name`,`last_name`,`email`,`password`,`status`,`approval_token`,`reset_token`,`access_control`) VALUES (5,'Xue','Feng','xf@gmail.com','Abcd!234',NULL,NULL,NULL,NULL);
INSERT INTO `users_profiles` (`id`,`first_name`,`last_name`,`email`,`password`,`status`,`approval_token`,`reset_token`,`access_control`) VALUES (6,'ck','kk','ck@sg.ibm.com','Abcd!234','Revoke',NULL,NULL,'Reader');
